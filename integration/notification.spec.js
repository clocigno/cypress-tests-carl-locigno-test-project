/**
 * @author Ben Hayward
 * @desc Spec tests for comment threads.
 */
import generateRandomId from '../support/utilities';

context('Notification', () => {
  //secondary user for testing.
  const username = generateRandomId();
  const password = generateRandomId() + 'X#';

  const commentText = generateRandomId();
  const postText = generateRandomId();
  const notificationBell = '.m-notificationsTopbarToggle--newNav';
  const notification = 'minds-notification';
  /**
   * Before all, generate username and password, login as the new user and log out.
   * Next login to env user, make a post, log out. Finally, login into new user
   */
  before(() => {
    cy.newUser(username, password, true);
    cy.logout();
    cy.login(true);
    cy.post(postText);
    cy.logout();
    cy.login(true, username, password);
  });

  // Delete new user after tests run
  after(() => {
    cy.deleteUser(username, password);
    cy.clearCookies();
  });

  // Set up alias for requesting notifications and check to see that the location path is /newsfeed/subscriptions
  beforeEach(() => {
    cy.server();
    cy.route('GET', '**/api/v1/notifications/all**').as('notifications');
    cy.location('pathname').should('eq', '/newsfeed/subscriptions');
  });

  it.skip('should close notifications flyout when clicking a notification', () => {
    // Comment on generated env user's post (To create a notification for env user).
    cy.comment(commentText);
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications');
    // Click on the first notification.
    cy.get(notification).within($list => {
      cy.get('a')
        .first()
        .click();   
    });
    // The notifications flyout should not be visible.
    cy.get(notification).should('not.be.visible');
  });

  it.skip('should alert the user that a post has been commented on', () => {
    // Comment on generated env user's post.
    cy.comment(commentText);
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications')
      .then(xhr => {
        expect(xhr.status).to.equal(200);
    });
    // Check the first notifcation.
    cy.get(notification)
      .first()
      .click();
    // It should contain the comment text.
    cy.contains(commentText);
  });

  it.skip('should alert the user that they have been tagged in a post', () => {
    // Post, tagging the env user
    const taggingPostText = '@' + Cypress.env().username + ' ' + postText;
    cy.post(taggingPostText);
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications')
      .then(xhr => {
        expect(xhr.status).to.equal(200);
    });
    // Click on the first notifcation.
    cy.get(notification)
      .first()
      .click();
    // It should contain the post text with the tag.
    cy.contains(taggingPostText);
  });

  it.skip('should alert the user when they have been subscribed to', () => {
    // Subscribe to env user
    cy.subscribe();
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications')
      .then(xhr => {
        expect(xhr.status).to.equal(200);
    });
    // Check the first notifcation and check for subscription text
    cy.get(notification)
      .first()
      .contains(' subscribed to you ')
  });

  it.skip('should alert the user their post has been upvoted', () => {
    // Upvote the env user's post
    cy.upvote();
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications')
      .then(xhr => {
        expect(xhr.status).to.equal(200);
    });
    // Check the first notifcation and check for upvote text
    cy.get(notification)
      .first()
      .contains(' voted up ')
  });

  it.skip('should alert the user their post has been reminded', () => {
    // Remind  the env user's post
    cy.remind();
    // Logout, log into env user.
    cy.logout();
    cy.login(true);
    // Open their notifications.
    cy.get(notificationBell)
      .click()
      .wait('@notifications')
      .then(xhr => {
        expect(xhr.status).to.equal(200);
    });
    // Check the first notifcation and check for remind text
    cy.get(notification)
      .first()
      .contains(' reminded ')
  });
});
