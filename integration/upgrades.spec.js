import generateRandomId from '../support/utilities';

context('Upgrades page', () => {

  const user = {
    username: generateRandomId(),
    password: generateRandomId()+'aA1!'
  }

  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (!sessionCookie) {
        return cy.logout();
      }
    });
    cy.logout();
    cy.newUser(user.username, user.password);
  });

  // after(()=> {
  //   cy.deleteUser(user.username, user.password);
  // });

  beforeEach(() => {
    cy.preserveCookies();
    cy.visit('/upgrades');
  });

  it('should scroll to upgrades table', () => {
    cy.viewport(1200, 600); // Only on desktop

    const scrollButton = '[data-cy="m-upgrades__upgrade-now-button"]';
    const heading = '.m-upgradesUpgradeOptions__header h2';

    cy.get(scrollButton)
      .should('contain', 'Upgrade Now')
      .click();

    cy.wait(1500);
    cy.isInViewport(heading);
  });

  it('should have the ability to trigger pre Buy Tokens modal phone verification', () => {
    cy.intercept('GET', '**api/v2/blockchain/purchase**').as('purchaseGET');
    cy.get('.m-upgradesBuyTokens__wrapper').within($list => {
      cy.get('m-button').click();
    })

    cy.get('m-concertcolors__modal').should('exist');
    cy.get('.m-buyTokens__body').should('exist');
  });

  it('should navigate to Nodes', () => {
    const upgradeButton = cy.get(
      '[data-cy="m-upgradeOptions__contact-us-nodes-button"]'
    );

    upgradeButton
      .click()
      .location('pathname').should('contain', '/nodes');
  });

  // TODO: Make new user for tests
  it('should navigate to Plus and Pro and trigger wires', () => {
    const upgradeButton = cy.get(
      '[data-cy="m-upgradeOptions__upgrade-to-plus-button"]'
    );

    upgradeButton.click();

    cy.location('pathname').should('contain', '/plus');
  });

  // TODO: Make new user for tests
  it('should navigate to Pro and trigger a Wire', () => {
    const upgradeButton = cy.get(
      '[data-cy="m-upgradeOptions__upgrade-to-pro-button"]'
    );

    upgradeButton.click();

    cy.location('pathname').should('contain', '/pro');
  });
});
