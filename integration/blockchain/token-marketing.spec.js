context('Token Page', () => {
  const buyButton = '[data-cy=data-minds-join-rewards-button] button div';

  before(() => {
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (!sessionCookie) {
        return cy.login(true);
      }
    });
  });

  beforeEach(() => {
    cy.preserveCookies();
    cy.visit('/token');
  });

  it('should have the ability to trigger Buy Tokens modal', () => {
    cy.get(buyButton)
      .should('be.visible')
      .contains('Buy Tokens')
      .click();

    cy.get('.m-web3Modal').should('be.visible');
  });
});
