import generateRandomId from '../../support/utilities';

/**
 * @author Ben Hayward
 * @create date 2019-08-09 14:42:51
 * @desc Spec tests for comment threads.
 */
context.only('Comment Threads', () => {
  const postText = generateRandomId();
  const commentPostButton = '.m-commentPoster__postButton';
  const textArea = 'minds-textarea';
  const commentBlock = '.minds-block';
  const viewMore = '.m-comments__viewMore';

  before(() => {
    //make a post new.
    cy.getCookie('minds_sess').then(sessionCookie => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });
  
    // This test makes use of cy.post()
    cy.overrideFeatureFlags({ 'activity-composer': true });
    cy.location('pathname').should('eq', `/newsfeed/subscriptions`);

    cy.post(postText);
  });

  beforeEach(() => {
    cy.preserveCookies();
    cy.server();
    cy.intercept('POST', '**/api/v1/comments/**').as('postComment');
    cy.rateLimitBypass();
  });

  it('should post three tiers of comments', () => {
    // within of post body
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        // type comment
        cy.get(textArea).type("test 1");

        // hit post button.
        cy.get(commentPostButton)
          .click()
          .wait('@POSTComment');

        // within new nested comment block (t2)
        cy.get(commentBlock)
          .within($list => {
            // click reply
            cy.contains('Reply').click();

            // type comment and post
            cy.get(textArea).type("test 2");
            cy.get(commentPostButton).click().wait('@POSTComment');

            // within new nested comment block (t3)
            cy.get(commentBlock)
              .within($list => {
                // click reply
                cy.contains('Reply').click();

                // type comment and post
                cy.get(textArea).type("test 3");
                cy.get(commentPostButton).click().wait('@POSTComment');
              })
          });


      });
  });

  it('should paginate correctly', () => {
    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.intercept('POST', '**/api/v1/comments/**').as('POSTComment');

        for (let i = 0; i < 25; i++) {
          cy.get(textArea).last().type(`comment n°${i}`);
          cy.get(commentPostButton).last().click().wait('@POSTComment');
        }

        cy.get('m-relativetimespan').click();
      });

    cy.contains(postText)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.intercept('GET', '**/api/v2/comments/**').as('GETcomments')
        cy.get(viewMore).click().wait('@GETcomments');
        cy.get(viewMore).click().wait('@GETcomments');
      });
  });


});
