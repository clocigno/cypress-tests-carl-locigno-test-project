/**
 * @author Ben Hayward
 * @desc E2E testing for Minds Boost Console pages.
 */
import generateRandomId from '../../support/utilities';

context('Boost Modal', () => {
  const activityBoostButton = '[data-cy=data-minds-activity-boost-button]';

  const boostViewsInput = "[data-cy=data-minds-boost-modal-views-input]";
  const tokensInput = "[data-cy=data-minds-boost-modal-tokens-input]";
  const amountInputError ="[data-cy=data-minds-boost-modal-amount-error]";
  
  const boostPostButton = "[data-cy=data-minds-boost-modal-boost-button] button";

  const paymentSelector = "[data-cy=data-minds-boost-modal-method-select]";
  const targetInput = "[data-cy=data-minds-boost-modal-target-input]";

  const offersTab = "[data-cy=data-minds-boost-modal-offers-tab]";

  const channelBoostButton = "m-channelActions__boost m-button";
  
  before(() => {
    // This test makes use of cy.post()
    cy.overrideFeatureFlags({ 'boost-modal-v2': true });

    cy.getCookie('minds_sess').then(sessionCookie => {
      if (sessionCookie === null) {
        return cy.login(true);
      }
    });

    cy.intercept('GET', '**/api/v1/channel/**').as('GETChannel');

    cy.navToChannel();
  });

  beforeEach(() => {
    cy.preserveCookies();
  });

  after(() => {
    // revoke all boosts
    cy.get('.m-dropdown .minds-avatar').click();
    cy.contains('Boost Console').click();

    revokeAll('newsfeed');

    cy.get('.m-topbar--navigation').within($list => {
      cy.contains('Sidebar').click();
    });

    
    revokeAll('content');
  
    cy.get('.m-topbar--navigation').within($list => {
      cy.contains('Offers').click();
    });

    cy.contains('Outbox').click()

    revokeAll('peer');
  });

  it('should allow the user to make a valid newsfeed boost', () => {
    newBoost();
    // select offchain.
    cy.get(paymentSelector).select('offchain');
    cy.get(boostPostButton).should('be.enabled');

    // set below min views and check.
    cy.get(boostViewsInput).clear().type(0);

    cy.get(boostPostButton).should('be.disabled');
    cy.get(amountInputError).should('contain', 'you may only boost for a minimum of');

    // set to valid minimum views and check error is gone.
    cy.get(boostViewsInput).clear().type(1000);

    cy.get(boostPostButton).should('not.be.disabled');
    cy.get(amountInputError).should('not.contain', 'you may only boost for a maximum of');
    cy.get(amountInputError).should('not.contain', 'you may only boost for a minimum of');

    cy.intercept('POST', '**/api/v2/boost/activity/**').as('POSTBoost')
    cy.get(boostPostButton).click().wait('@POSTBoost');
    cy.contains('Success! Your boost request is being processed.')
  });

  it('should allow the user to make a valid offer boost', () => {
    newBoost();
    cy.get(offersTab).click();

    // select offchain.
    cy.get(paymentSelector).select('offchain');
    cy.get(boostPostButton).should('not.be.enabled');

    // set to valid minimum views and check error is gone.
    cy.get(tokensInput).invoke('val', 1).trigger('change');

    // set target
    cy.get(targetInput).type('minds');

    // click top option
    cy.get('.m-boostOfferTarget__matchesListItem').first().click();
    cy.get(boostPostButton).should('not.be.disabled');

    cy.intercept('POST', '**/api/v2/boost/peer/**').as('POSTPeerBoost')
    cy.get(boostPostButton).click().wait('@POSTPeerBoost');
    cy.contains('Success! Your boost request is being processed.')    
  });

  it('should allow the user to make a valid channel boost', () => {    
    cy.get(channelBoostButton).click();

    // select offchain.
    cy.get(paymentSelector).select('offchain');
    cy.get(boostPostButton).should('be.enabled');

    // set to valid minimum views and check error is gone.
    cy.get(boostViewsInput).invoke('val', 1000).trigger('change');
  
    cy.intercept('POST', '**/api/v2/boost/user/**').as('POSTUserBoost')
    cy.get(boostPostButton).click().wait('@POSTUserBoost');
    cy.contains('Success! Your boost request is being processed.')    
  });

  const newBoost = () => {
    const postContent = 'Test boost, please reject...' + generateRandomId();
    cy.post(postContent);
    
    cy.contains(postContent)
      .parentsUntil('m-activity')
      .parent()
      .within($list => {
        cy.get(activityBoostButton)
          .click();
      });
  }

  const revokeAll = (type) => {
    cy.intercept('DELETE', '**api/v2/boost/newsfeed/*/revoke').as('DELETENewsfeedBoost');
    cy.intercept('DELETE', '**/api/v2/boost/content/*/revoke').as('DELETEContentBoost');
    cy.intercept('DELETE', '**api/v2/boost/peer/*/revoke').as('DELETEPeerBoost');

    let alias;

    switch(type) {
      case 'newsfeed':
        alias = '@DELETENewsfeedBoost';
        break;
      case 'content':
        alias = '@DELETEContentBoost';
        break;
      case 'peer':
        alias = '@DELETEPeerBoost';
        break;  
    }
    cy.get('.m-boostCardManagerButton--revoke').each(($el, index, $list) => {      
      cy.wrap($el).scrollIntoView().click().wait(alias);
    });
  }
});
